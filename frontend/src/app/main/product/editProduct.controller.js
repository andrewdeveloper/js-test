(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('editProductController', editProductController);

  /** @ngInject */
  function editProductController($scope, Restangular, $state, $stateParams, $http) {
    $scope.addRemoveCategory = function (categoryId) {
      let index = $scope.product.categories.indexOf(categoryId);
      if( index == -1) {
        $scope.product.categories.push(categoryId);
      } else {
        $scope.product.categories.splice(index, 1);
      }
    };
    Restangular.all('products?id=' + $stateParams.productID).getList()
      .then(function(products) {
        $scope.product = products[0];
        Restangular.all('categories').getList()
          .then(function(categories) {
            $scope.categories = categories;
            for(let i = 0; i < $scope.product.categories.length; i++) {
              for(let j = 0; j < $scope.categories.length; j++) {
                if ($scope.product.categories[i] === $scope.categories[j].id) {
                  $scope.categories[j].check = true;
                }
              }
            }
          });
      });
    $scope.save = function (product) {
      product.categories = JSON.stringify(product.categories).replace("[","{").replace("]", "}");
      $http({
        method: 'PUT',
        url: 'http://localhost/api/v1/products/',
        data: product
      }).then(function successCallback(response) {
        console.log(response);
        $state.go('home', {reload: true});
      }, function errorCallback(response) {
        console.log(response);
      });
    }
  }
})();
