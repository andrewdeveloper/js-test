(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('newProductController', newProductController);

  /** @ngInject */
  function newProductController($scope, Restangular, $state) {
    $scope.product = {};
    $scope.product.categories = [];
    $scope.checkbox = [];
    $scope.addRemoveCategory = function (categoryId) {
      var index = $scope.product.categories.indexOf(categoryId);
      if( index == -1) {
        $scope.product.categories.push(categoryId);
      } else {
        $scope.product.categories.splice(index, 1);
      }
      console.log($scope.product.categories)
    };
    Restangular.all('categories').getList()
      .then(function(categories) {
        $scope.categories = categories;
      });
    $scope.save = function (product) {
      Restangular.all('products').getList()
        .then(function(products) {
          console.log(product);
          products.post(product);
          $state.go('home', {reload: true, inherit: false, notify: true})
        });
    };
  }
})();
