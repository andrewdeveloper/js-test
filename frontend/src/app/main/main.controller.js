(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, $stateParams, Restangular, $state) {
    $scope.goToCategory = function (id) {
      $state.go('home.category', {categoryID : id, reload: true});
    };
    $scope.goToEditCategory = function (id) {
      $state.go('home.editCategory', {categoryID : id, reload: true});
    };
    $scope.goToEditProduct = function (id) {
      $state.go('home.editProduct', {productID : id, reload: true});
    };
    $scope.goToNewProduct = function () {
      $state.go('home.newProduct', {reload: true});
    };
    $scope.goToNewCategory = function () {
      $state.go('home.newCategory', {reload: true});
    };
    $scope.categoryID = $stateParams.categoryID;
    Restangular.all('products').getList()
      .then(function(products) {
        $scope.products = products;
      });
    Restangular.all('categories').getList()
      .then(function(categories) {
        $scope.categories = categories;
      });
  }
})();
