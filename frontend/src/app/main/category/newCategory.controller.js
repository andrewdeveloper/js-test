(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('newCategoryController', newCategoryController);

  /** @ngInject */
  function newCategoryController($scope, Restangular, $state) {
    $scope.category = {};
    $scope.save = function (category) {
      Restangular.all('categories').getList()
        .then(function(categories) {
          categories.post(category);
          $state.go('home', {reload: true, inherit: false, notify: true});
        });
    };
  }
})();
