(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('CategoryController', CategoryController);

  /** @ngInject */
  function CategoryController($scope, Restangular, $state, $stateParams) {
    $scope.goToEditProduct = function (id) {
      $state.go('home.editProduct', {productID : id, reload: true});
    };
    Restangular.all('category-products/' + $stateParams.categoryID).getList()
      .then(function(products) {
        $scope.products = products;
      });
  }
})();
