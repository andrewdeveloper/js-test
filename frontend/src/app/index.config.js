(function() {
  'use strict';

  angular
    .module('frontend')
    .config(config);

  /** @ngInject */
  function config(RestangularProvider) {
    RestangularProvider.setBaseUrl('http://localhost/api/v1');
  }
})();
