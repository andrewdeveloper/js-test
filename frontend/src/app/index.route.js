(function() {
  'use strict';

  angular
    .module('frontend')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('home.category', {
        url: 'category/:categoryID',
        templateUrl: 'app/main/category/category.html',
        controller: 'CategoryController',
        controllerAs: 'category'
      })
      .state('home.newCategory', {
        url: 'new-category',
        templateUrl: 'app/main/category/newCategory.html',
        controller: 'newCategoryController',
        controllerAs: 'newCategory'
      })
      .state('home.editProduct', {
        url: 'product/:productID',
        templateUrl: 'app/main/product/editProduct.html',
        controller: 'editProductController',
        controllerAs: 'editProduct'
      })
      .state('home.newProduct', {
        url: 'new-product',
        templateUrl: 'app/main/product/newProduct.html',
        controller: 'newProductController',
        controllerAs: 'newProduct'
      });
    $urlRouterProvider.otherwise('/');
  }
})();
