/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are split into several files in the gulp directory
 *  because putting it all here was too long
 */

'use strict';

var gulp = require("gulp");
var fs = require('fs');
var uglify = require("gulp-uglify");
var babel  = require('gulp-babel');

gulp.task("scripts", function() {
  return gulp.src(["./gulp-crash-test.js"])
    .pipe(babel({presets: ['es2015']}))
    .pipe(concat("gulp-crash-test.minjs"))
    .pipe(uglify().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest("./"))
});
/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
fs.readdirSync('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});


/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
