const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const connection = require('./connection');
const routes = require('./routes');
const app = express();
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(cors());
app.use(express.static('public'));
app.use(express.static('files'));
app.use('/', express.static('public'));
connection.init();
routes.configure(app);
const server = app.listen(80, function() {
    console.log('Server listening on port ' + server.address().port);
});