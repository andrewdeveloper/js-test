const connection = require('./connection');
module.exports = {
    configure: function(app) {
        app.get('/api/v1/categories/', function(req, res) {
            connection.acquire(function(err, client, done) {
                client.query('select * from categories', function(err, result) {
                    done();
                    res.send(result.rows);
                });
            });
        });
        app.post('/api/v1/categories/', function(req, res) {
            connection.acquire(function(err, client, done) {
                client.query('INSERT INTO categories (name) values($1)', [req.body.name], function(err, result) {
                    done();
                    if (err) {
                        res.send({status: 1, message: 'CATEGORIES creation failed ' + err});
                    } else {
                        res.send({status: 0, message: 'CATEGORIES created successfully', result});
                    }
                });
            });
        });
        app.get('/api/v1/products/', function(req, res) {
            connection.acquire(function(err, client, done) {
                if (req.query.id) {
                    client.query('select * from products where products.id = ' + req.query.id, function(err, result) {
                        done();
                        res.send(result.rows);
                    });
                } else {
                    client.query('select * from products', function(err, result) {
                        done();
                        res.send(result.rows);
                    });
                }
            });
        });
        app.post('/api/v1/products/', function(req, res) {
            connection.acquire(function(err, client, done) {
                const query = 'INSERT INTO products (name, categories, price) values($1, $2, $3)';
                client.query(query, [req.body.name, req.body.categories, req.body.price], function(err, result) {
                    done();
                    if (err) {
                        res.send({status: 1, message: 'Product creation failed ' + err});
                    } else {
                        res.send({status: 0, message: 'Product created successfully', result});
                    }
                });
            });
        });
        app.put('/api/v1/products/', function(req, res) {
            connection.acquire(function(err, client, done) {
                const query = 'UPDATE products SET name=($1), categories =  ARRAY[]::int[] || ($2), price=($3) WHERE id=($4)';
                client.query(query, [req.body.name, req.body.categories, req.body.price, req.body.id], function(err, result) {
                    done();
                    if (err) {
                        res.send({status: 1, message: 'Product updated failed ' + err});
                    } else {
                        res.send({status: 0, message: 'Product updated successfully', result});
                    }
                });
            });
        });
        app.get('/api/v1/category-products/:id', function(req, res) {
            connection.acquire(function(err, client, done) {
                client.query('select * from products where ' + req.params.id + ' = ANY(products.categories::int[])', function(err, result) {
                    done();
                    res.send(result.rows);
                });
            });
        });
    }
};