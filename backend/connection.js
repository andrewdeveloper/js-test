const pg = require('pg');
const config = {
    user: 'andreinastas',
    database: 'catalog',
    password: 'superuser1',
    host: 'shopde.webfactional.com',
    port: 5432,
    max: 10,
    idleTimeoutMillis: 30000
};
function Connection() {
    this.pool = null;
    this.init = function() {
        this.pool = new pg.Pool(config);
    };
    this.acquire = function(callback) {
        this.pool.connect(function(err, client, done) {
            callback(err, client, done);
        });
    };
}
module.exports = new Connection();